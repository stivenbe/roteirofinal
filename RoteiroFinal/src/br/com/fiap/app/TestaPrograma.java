package br.com.fiap.app;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.fiap.dao.ProfessorDAO;
import br.com.fiap.modelo.Aluno;
import br.com.fiap.modelo.Materia;
import br.com.fiap.modelo.Professor;

public class TestaPrograma {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("RoteiroFinal");
		EntityManager em = emf.createEntityManager();
		ProfessorDAO dao = new ProfessorDAO(em);
		
		try {
			//Professor
			Professor professor = new Professor();
			professor.setId(1);
			professor.setNome("Michel");
			professor.setEndereco("Avenida Lins");
			professor.setCurso("Java JPA");
			professor.setMatricula(1234);
			
			//Aluno
			Aluno aluno = new Aluno();
			aluno.setId(1);
			aluno.setNome("Stiven");
			aluno.setEndereco("Rua dos Pinheiros");
			aluno.setCurso("Java JPA");
			aluno.setRm(48318);
			aluno.setProfessor(professor);
			
			//Materias
			Materia materia = new Materia();
			materia.setNome("JPA");
			materia.setCurso("Java JPA");
			materia.setAluno(aluno);
			
			Materia materia2 = new Materia();
			materia2.setNome("JDBC");
			materia2.setCurso("Java JPA");
			materia2.setAluno(aluno);
			
			aluno.getMateria().add(materia);
			aluno.getMateria().add(materia2);
			
			professor.getAluno().add(aluno);
			
			dao.salvar(professor);
			
			System.out.println("Professor Incluido com Sucesso! :)");

		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			em.close();
			emf.close();
		}
	}
}
