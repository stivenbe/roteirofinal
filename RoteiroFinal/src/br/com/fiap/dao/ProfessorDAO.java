package br.com.fiap.dao;

import javax.persistence.EntityManager;

import br.com.fiap.modelo.Professor;

public class ProfessorDAO {

	private EntityManager em;
	
	public ProfessorDAO(EntityManager em) {
		this.em = em;
	}
	
	public void salvar(Professor professor) {
		try {
			em.getTransaction().begin();
			em.persist(professor);
			em.getTransaction().commit();
		}catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		}
	}
}
